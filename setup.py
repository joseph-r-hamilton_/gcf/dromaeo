
import subprocess

import setuptools
import distutils.command.install_data

# Shoehorn Installation Script

output = subprocess.check_output(
	['./install.sh']
).decode('utf-8')
with open('/usr/bin/7zb','w') as f:
	f.write(output)


# Vanilla Setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dromaeo",
    version="0.0.1",
    author="Joseph Hamilton",
    author_email="joseph.r.hamilton@gmail.com",
    description="GCF Shoehorn",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
	
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
	"License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
    ],
)
