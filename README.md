# Dromaeo

## Description

A python module to shoehorn a configuration into a Google Cloud Function

This module serves no other purpose.

Do not "install" it on a normal python system or virtual environment and expect good results.

## Installation Details

### p7zip

The GCF Execution Environment lacks the 7z utility.

This installs p7zip to provide that.

### gcsfuse

This installs gcsfuse for access to Google Cloud Storage buckets.



